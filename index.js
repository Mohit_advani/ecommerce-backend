import express from "express";
import jsonServer from "json-server";
import auth from "json-server-auth";
import cors from 'cors';

const server = express();

server.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', '*');
  next();
});

var allowlist = ['https://sl-purityplants.netlify.app'];
var corsOptionsDelegate = function (req, callback) {
  var corsOptions;
  if (allowlist.indexOf(req.header('Origin')) !== -1) {
    corsOptions = { origin: true } // reflect (enable) the requested origin in the CORS response
  } else {
    corsOptions = { origin: false } // disable CORS for this request
  }
  callback(null, corsOptions) // callback expects two parameters: error and options
}

const router = jsonServer.router('./data/db.json');
server.use('/api', router);
server.db = router.db;
server.use(cors(corsOptionsDelegate));
const middlewares = jsonServer.defaults();

const rules = auth.rewriter({
  products: 444,
  "featured-products": 444,
  users: 600,
  orders: 660
});

server.use(rules);
server.use(auth);
server.use(middlewares);
server.use(router);

server.listen(8000);
